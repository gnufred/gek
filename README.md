WordPress theme powering https://goergokino.com

Use http://underscores.me/ as a base

# Licenses

Code (html, css, js) is GPLv2: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

Graphics and branding content are: All copyright reserved to « La clinique Go Ergo Kino .senc ». Do not use without permissions.
