<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Go_Ergo_Kino
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div class="site-logo">
				<img src="<?php echo get_template_directory_uri(); ?>/imgs/footer-logo.png" alt="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
			</div>
			<div class="footer-text">
				<?php get_content_by_path( 'contenus-du-theme/pied-de-page/texte' ); ?>
			</div>
			<div class="footer-info">
				<?php get_content_by_path( 'contenus-du-theme/pied-de-page/infos' ); ?>
			</div>
		</div>
		<div class="site-links">
			<div class="site-links-content">
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'gek' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'gek' ), 'WordPress' ); ?></a>
				<span class="sep"> | </span>
				<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'gek' ), 'gek', '<a href="https://akeif.me" rel="designer">Frédéric Potvin</a>' ); ?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
